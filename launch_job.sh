#!/bin/bash
#SBATCH --partition=cpu-generic
#SBATCH --ntasks=1
#SBATCH --mem=15000
#SBATCH --job-name="minbias_production"
#SBATCH --output=minbias_production-srun.out

RANDOM_SEED=130
echo "Job started" 
bash run_events_mb.sh $RANDOM_SEED
echo "All Done!"

