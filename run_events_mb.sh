# PYTHIA_CARD=hardQCD_400_pileup.cmd
PYTHIA_CARD=qg2Hq_400_pileup.cmd
DETECTOR_CARD=CMS_PhaseII_200PU_v04.tcl
FILENAME="qg2hq_MinBias_PU200_test_seed${1}.root"
DIR=/storage/DSIP/physics/pseudo_detector
PWD=/home/adiluca/Projects/pseudo_detector/pileup_study
SEED=${1}
echo $SEED
sed -i "/Random:seed =/c\Random:seed =${SEED}" "${PWD}/delphes_cards/pythia8/${PYTHIA_CARD}"

sed -i "/set RandomSeed/c\set RandomSeed ${SEED}" "${PWD}/delphes_cards/detectors/${DETECTOR_CARD}"

PYTHIA_CARD="/home/temp/delphes_cards/pythia8/${PYTHIA_CARD}"
DETECTOR_CARD="/home/temp/delphes_cards/detectors/${DETECTOR_CARD}"
udocker run -v $PWD:/home/temp -v $DIR:/home/out dumper_udocker /bin/bash -c "DelphesPythia8 ${DETECTOR_CARD} ${PYTHIA_CARD} /home/out/${FILENAME}"