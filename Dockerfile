FROM hepstore/rivet:3.1.2

RUN apt-get update -y \
    && apt-get install -y \
      libxft2 libxpm4  libpthread-stubs0-dev libsqlite3-dev uuid-dev \
    && apt-get -y autoclean \
    && cd /usr/local \
    && wget --no-verbose https://root.cern/download/root_v6.20.06.Linux-ubuntu20-x86_64-gcc9.3.tar.gz -O- | tar xz \
	&& echo /usr/local/root/lib >> /etc/ld.so.conf \
	&& ldconfig

ENV ROOTSYS /usr/local/root
ENV PATH $ROOTSYS/bin:$PATH
ENV PYTHONPATH $ROOTSYS/lib:$PYTHONPATH
ENV CLING_STANDARD_PCH none

ENV LD_LIBRARY_PATH /usr/local/lib

RUN mkdir /code && cd /code \
    && wget --no-verbose https://lhapdf.hepforge.org/downloads/?f=LHAPDF-6.3.0.tar.gz -O- | tar xz \
    && cd LHAPDF-*/ && ./configure --prefix=/usr/local \
    && make -j $(nproc --ignore=1) && make install \
    && cd ../.. && rm -r /code

RUN lhapdf install NNPDF30_nlo_as_0118 NNPDF23_lo_as_0130_qed

RUN mkdir /code && cd /code \
    && apt-get -y install rsync && apt-get autoclean \
    && wget --no-verbose https://pythia.org/download/pythia83/pythia8305.tgz -O- | tar xz \
    && cd pythia*/ && ./configure --enable-shared --{prefix,with-{hepmc2,lhapdf6}}=/usr/local \
    && make -j $(nproc --ignore=1) && make install \
    && cd ../.. && rm -r /code

ENV PYTHIA8 /usr/local

RUN cd /usr/local/share \
	&& apt-get -y install subversion \ 
	&& git clone https://gitlab.cern.ch/POWHEG/POWHEG-BOX-V2

RUN	cd /usr/local/share/POWHEG-BOX-V2 \
	&& svn co  --username anonymous --password anonymous svn://powhegbox.mib.infn.it/trunk/User-Processes-V2/hvq \
	&& cd hvq && make pwhg_main 

RUN apt-get -y install libboost-all-dev \
	&& mkdir /code && cd /code \
	&& git clone https://gitlab.cern.ch/CLHEP/CLHEP.git && cd CLHEP/ \
	&& mkdir build && cd build \
	&& cmake -DCMAKE_INSTALL_PREFIX=/usr/local ../ \
	&& cmake --build . --config RelWithDebInfo && ctest && cmake --build . --target install \
	&& cd / && rm -r /code

RUN 	apt-get -y install tcl 

RUN cd /usr/local/share\
    && git clone https://github.com/delphes/delphes.git \
    && cd delphes\
    && make HAS_PYTHIA8=true\
    && cd
    
ENV DELPHES /usr/local/share/delphes

ENV LD_LIBRARY_PATH $DELPHES:$LD_LIBRARY_PATH

ENV ROOT_INCLUDE_PATH $DELPHES:$DELPHES/external:$ROOT_INCLUDE_PATH

RUN apt-get install -y pkg-config \
    && mkdir /code && cd /code \
    && git clone https://github.com/WolfgangWaltenberger/rave.git && cd rave\
    && ./bootstrap && ./configure CXXFLAGS=-std=c++11 --disable-java \
    && make -j $(nproc --ignore=1) && make install \
    && cd ..

ENV RAVE /usr/local/include/rave/impl/
ENV LD_LIBRARY_PATH $RAVE:$LD_LIBRARY_PATH

ENV DelphesSYS /usr/local/share/delphes
ENV RaveSYS /code/rave/src/.libs/
ENV LD_LIBRARY_PATH $RaveSYS:$LD_LIBRARY_PATH

ENV PATH="${DELPHES}:${PATH}"
